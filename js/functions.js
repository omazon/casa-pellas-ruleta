function getUrlVar(variable){
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
}
function getRandonStr(lenght){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < lenght; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
function showResult(status,premio,error) {
    if(status == true){
        var contenido = '<p>El modelo cotizado es: ' + modelo + '</p>' +
            '<p>El código es: <code>' + codigo + '</code></p>' +
            '<p>El premio es: ' + premio + '</p>';
    }else{
        var contenido = 'Ha ocurrido un error, inténtalo de nuevo más tarde:' + error;
    }
    loading('','none');
    document.querySelector('#resultado').innerHTML = contenido;
}
function switchImage(){
    var img_modelo = document.querySelector('#img-modelo');
    switch (modelo){
        case 'yaris':
            img_modelo.setAttribute('src','./logo/yaris.png')
            break;
        default:
    };
}
function sendData(premio){
    axios.post(root +'/posts', {
        marca: modelo,
        code: codigo,
        premio: premio
    })
        .then(function () {
            showResult(true,premio);
        })
        .catch(function (error) {
            showResult(false,premio,error);
        });
}

function getData(){
    axios.get(root +'/users')
        .then(function (response) {
             makeOptions(response.data);
        })
        .catch(function (error) {
            loading('Ha ocurrido un error, favor inténtalo más tarde. '+error,'block');
        });
}

function makeOptions(value){
    var lenght = Object.keys(value).length;
    var opciones = [];
    for(i=0;i < lenght; i++){
        opciones.push(value[i].username);
    }
    window.options = opciones;
    ruleta();
}
function loading(mensaje,estilo){
    var loading = document.querySelector('#loading');
    loading.innerHTML = mensaje;
    loading.style.display = estilo;
}